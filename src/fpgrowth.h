/*----------------------------------------------------------------------
  File    : fpgrowth.h
  Contents: fpgrowth algorithm for finding frequent item sets
  Author  : Christian Borgelt
  History : 2011.08.22 file created
            2011.09.21 available variants and modes reorganized
----------------------------------------------------------------------*/
#ifndef __FPGROWTH__
#define __FPGROWTH__
#include "tract.h"
#ifndef ISR_CLOMAX
#define ISR_CLOMAX
#endif
#include "report.h"

/*----------------------------------------------------------------------
  Preprocessor Definitions
----------------------------------------------------------------------*/
/* --- fpgrowth variant --- */
#define FPG_AUTO      0         /* automatic choice based on density */
#define FPG_SIMPLE    1         /* simple  nodes (parent/link) */
#define FPG_COMPLEX   2         /* complex nodes (children/sibling) */
#define FPG_SINGLE    3         /* top-down processing on single tree */
#define FPG_TOPDOWN   4         /* top-down processing of the tree */

/* --- operation modes --- */
#define FPG_PERFECT   0x0100    /* perfect extension pruning */
#define FPG_FIM16     0x0200    /* use 16 items machine (bit rep.) */
#define FPG_REORDER   0x0400    /* reorder items in cond. databases */
#define FPG_TAIL      0x0800    /* head union tail pruning */
#define FPG_DEFAULT   (FPG_PERFECT|FPG_FIM16|FPG_REORDER|FPG_TAIL)

#define FPG_VERBOSE   ((INT_MIN >> 1) & ~INT_MIN)

/*----------------------------------------------------------------------
  Functions
----------------------------------------------------------------------*/
#ifdef NOMAIN
extern int fpgrowth (TABAG *tabag, int algo, int target, int mode,
                     int supp,int eval, double thresh,
                     ISREPORT *report);
#endif
#endif
